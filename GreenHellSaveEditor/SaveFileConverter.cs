﻿
using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Reflection;
using System.Linq;
using System.Text;
//using System.Runtime.InteropServices.Marshalling;

namespace Enums
{
    public enum GameMode
    {
        None,
        Story,
        Survival,
        Debug,
        PVE
    }

}

namespace GreenHellSaveEditor
{

    [Serializable]
    public class GameVersion
    {
        public int m_Major;

        public int m_Minor;

        public GameVersion() { }

    }

    //It's pretty illegal to distribute the game's DLL with my code, and the way BinaryFormatter works, it tries to grab a DLL to resolve the type
    //While I could create a DLL with the same name, I don't want people to think I included their code without their consent. Which it will look like at first glance
    //So instead I redirect the BinaryFormatter type binder to return a different type when it finds Assembly-CSharp
    //This link helped: https://learn.microsoft.com/en-us/archive/msdn-technet-forums/e5f0c371-b900-41d8-9a5b-1052739f2521
    public class AssemblyRebinder : SerializationBinder
    {

        public override Type? BindToType(string assemblyName, string typeName)
        {
            System.Diagnostics.Debug.WriteLine(assemblyName);
            System.Diagnostics.Debug.WriteLine(typeName);

            //We don't want the version number and all that crap, just the name
            string properAssemblyName = assemblyName.Split(',')[0];

            if (properAssemblyName == "Assembly-CSharp" ||
                properAssemblyName == "GreenHellGame")
            {
                System.Diagnostics.Debug.WriteLine("Found assembly with target name");
                System.Diagnostics.Debug.WriteLine($"{properAssemblyName} {typeName}");

                if(typeName == "GameVersion") return new GameVersion().GetType();
                if(typeName == "Enums.GameMode") return new Enums.GameMode().GetType();

                return Assembly.GetExecutingAssembly().GetType(typeName);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Non-target assmbly");
                System.Diagnostics.Debug.WriteLine($"{properAssemblyName} {typeName}");
            }


            //Get all locally loaded assemblies so we can bind types that are not our exceptions
            Assembly[] loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();

            //Loop over loaded assemblies and if it is loaded, return the type from that assembly
            foreach (Assembly assembly in loadedAssemblies)
            {
                if (properAssemblyName == assembly.FullName.Split(",")[0]) return assembly.GetType(typeName);

            }

            return null;
        }

    }
    public class SaveFileConverter
    {

        public static Dictionary<string, int> intVals = new Dictionary<string, int>();

        public static Dictionary<string, string> strVals = new Dictionary<string, string>();

        public static Dictionary<string, float> floatVals = new Dictionary<string, float>();

        public static Dictionary<string, bool> boolVals = new Dictionary<string, bool>();

        static GameVersion gameVersion;
        static Enums.GameMode gameMode;
        static long unknown1;
        static int unknown2;
        static int unknown3;
        static bool unknown4;
        static bool unknown5;
        static bool unknown6;
        static bool unknown7;
        static string unknown8;



        public static void Load(string saveFile)
        {

            //string str = System.Text.Encoding.UTF8.GetString(barr);
            //Console.WriteLine(str);


            /////
            ///

            BinaryFormatter binaryFormatter = new BinaryFormatter();

            byte[] array = System.IO.File.ReadAllBytes(saveFile);
            MemoryStream memoryStream = new MemoryStream(array);
            //Console.WriteLine(binaryFormatter.Deserialize(memoryStream));
            binaryFormatter.Binder = new AssemblyRebinder();
            gameVersion = (GameVersion)binaryFormatter.Deserialize(memoryStream);
            //Game disposes of these values for some reason
            gameMode = (Enums.GameMode)binaryFormatter.Deserialize(memoryStream);
            unknown1 = (long)binaryFormatter.Deserialize(memoryStream);

            unknown2 = (int)binaryFormatter.Deserialize(memoryStream);

            ////
            //Don't know what this is
            unknown3 = (int)binaryFormatter.Deserialize(memoryStream);

            unknown4 = (bool)binaryFormatter.Deserialize(memoryStream);
            unknown5 = (bool)binaryFormatter.Deserialize(memoryStream);

            //Don't know why they do this for most recent versions
            unknown6 = (bool)binaryFormatter.Deserialize(memoryStream);
            unknown7 = (bool)binaryFormatter.Deserialize(memoryStream);
            //binaryFormatter.Deserialize(memoryStream);
            unknown8 = (string)binaryFormatter.Deserialize(memoryStream);


            intVals = (Dictionary<string, int>)binaryFormatter.Deserialize(memoryStream);
            strVals = (Dictionary<string, string>)binaryFormatter.Deserialize(memoryStream);
            floatVals = (Dictionary<string, float>)binaryFormatter.Deserialize(memoryStream);
            boolVals = (Dictionary<string, bool>)binaryFormatter.Deserialize(memoryStream);
            memoryStream.Close();

            /*File.WriteAllText("output.txt", unknown1.ToString());
            File.AppendAllText("output.txt", unknown2.ToString());
            File.AppendAllText("output.txt", unknown3.ToString());
            File.AppendAllText("output.txt", unknown4.ToString());
            File.AppendAllText("output.txt", unknown5.ToString());
            File.AppendAllText("output.txt", unknown6.ToString());
            File.AppendAllText("output.txt", unknown7.ToString());
            File.AppendAllText("output.txt", unknown8.ToString());
            File.AppendAllText("output.txt", string.Join(Environment.NewLine, intVals));
            File.AppendAllText("output.txt", string.Join(Environment.NewLine, strVals));
            File.AppendAllText("output.txt", string.Join(Environment.NewLine, floatVals));
            File.AppendAllText("output.txt", string.Join(Environment.NewLine, boolVals));*/

        }

        public static void DisableInsects()
        {
            if (intVals.ContainsKey("Difficulty_Version")) intVals["Difficulty_Version"] = 1;
            else intVals.Add("Difficulty_Version", 1);
            if (intVals.ContainsKey("Difficulty_ActivePresetType")) intVals["Difficulty_ActivePresetType"] = 5;
            else intVals.Add("Difficulty_ActivePresetType", 5);
            if (boolVals.ContainsKey("Difficulty_Insects")) boolVals["Difficulty_Insects"] = false;
            else boolVals.Add("Difficulty_Insects", false);
            if (boolVals.ContainsKey("Difficulty_Leeches")) boolVals["Difficulty_Leeches"] = false;
            else boolVals.Add("Difficulty_Leeches", false);
        }

        public static byte[] ByteStringReplace(byte[] byteStr, byte[] toReplace, byte[] replacement)
        {
            int targetMatches = toReplace.Length;
            int byteMatches = 0;
            int matchStart = 0;
            int toReplaceIndex = 0;
            bool found = false;

            for(int i = 0; i < byteStr.Length; i++)
            {
                if (byteStr[i] == toReplace[toReplaceIndex])
                {
                    byteMatches++;
                    if (matchStart == 0) matchStart = i;
                    if (byteMatches == targetMatches) {
                        found = true;    
                        break; 
                    }
                    toReplaceIndex++;

                }
                else
                {
                    byteMatches = 0;
                    matchStart = 0;
                    toReplaceIndex = 0;
                }
            }

            if (!found) return byteStr;

            List<byte> byteList = byteStr.ToList<byte>();
            byteList.RemoveRange(matchStart, targetMatches);
            byteList.InsertRange(matchStart, replacement);
            return byteList.ToArray();

        }

        public static byte[] ByteStringReplaceAll(byte[] byteStr, byte[] toReplace, byte[] replacement)
        {
            while(true)
            {
                byte[] newStr = ByteStringReplace(byteStr, toReplace, replacement);
                if (Enumerable.SequenceEqual<byte>(byteStr, newStr)) return byteStr;
                byteStr = newStr;

            }
        }
        public static void Save(string savePath)
        {

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            MemoryStream memoryStream = new MemoryStream();

            binaryFormatter.Serialize(memoryStream, gameVersion);
            binaryFormatter.Serialize(memoryStream, gameMode);
            binaryFormatter.Serialize(memoryStream, unknown1);
            binaryFormatter.Serialize(memoryStream, unknown2);
            binaryFormatter.Serialize(memoryStream, unknown3);
            binaryFormatter.Serialize(memoryStream, unknown4);
            binaryFormatter.Serialize(memoryStream, unknown5);
            binaryFormatter.Serialize(memoryStream, unknown6);
            binaryFormatter.Serialize(memoryStream, unknown7);
            binaryFormatter.Serialize(memoryStream, unknown8);
            binaryFormatter.Serialize(memoryStream, intVals);
            binaryFormatter.Serialize(memoryStream, strVals);
            binaryFormatter.Serialize(memoryStream, floatVals);
            binaryFormatter.Serialize(memoryStream, boolVals);

            //Here's the annoying part, we have to edit the save file to replace our types with Green Hell's...
            byte[] saveBytes = memoryStream.GetBuffer();


            saveBytes = ByteStringReplaceAll(saveBytes, Encoding.ASCII.GetBytes("JGreenHellSaveEditor"), Encoding.ASCII.GetBytes("FAssembly-CSharp"));
            saveBytes = ByteStringReplaceAll(saveBytes, Encoding.ASCII.GetBytes("Version=1.0.0.0"), Encoding.ASCII.GetBytes("Version=0.0.0.0"));
            
            //You'd have to look at the save file to understand this, but in short:
            //Not entirely sure why (because it's in byte format), but the byte before the GameVersion is 0F instead of 0B
            //Could be a version number, since the fact that they use BinaryFormatter at all shows they are using a really old C# version
            //Of course, the game freezes up if this isn't corrected
            List<byte> saveVersionToReplace = new List<byte>();
            saveVersionToReplace.Add(31);
            saveVersionToReplace.AddRange(Encoding.ASCII.GetBytes("GreenHellSaveEditor.GameVersion"));
            List<byte> saveVersionReplacement = new List<byte>();
            saveVersionReplacement.Add(11);
            saveVersionReplacement.AddRange(Encoding.ASCII.GetBytes("GameVersion"));
            saveBytes = ByteStringReplace(saveBytes, saveVersionToReplace.ToArray(), saveVersionReplacement.ToArray());

            System.IO.File.WriteAllBytes(savePath, saveBytes);


        }

        public static void Main(string[] args)
        {
            Load("K:\\c#\\DeserializeSaveFile\\MPSlot0.sav");
            Save("K:\\c#\\DeserializeSaveFile\\MPSlot0-modded.sav");
            Load("K:\\c#\\DeserializeSaveFile\\MPSlot0-modded.sav");

        }
    }
}