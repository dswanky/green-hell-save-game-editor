﻿using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Platform.Storage;
using Avalonia.Utilities;
using GreenHellSaveEditor;
using System.Linq;

namespace GreenHellSaveEditor.Views;

public partial class MainView : UserControl
{
    public MainView()
    {
        InitializeComponent();
    }

    string saveFile;
    string outFolder;

    public async void SelectFile(object sender, RoutedEventArgs e)
    {
        TopLevel? topLevel = TopLevel.GetTopLevel(this);
        if (topLevel != null)
        {
            var files = await topLevel.StorageProvider.OpenFilePickerAsync(new FilePickerOpenOptions
            {
                AllowMultiple = false,
                Title = "Open Green Hell Save File"
            });

            if(files.Count >= 1)
            {
                saveFile = SavePath.Text = files[0].TryGetLocalPath();
            }

        }

    }

    public async void SetOutFolder(object sender, RoutedEventArgs e)
    {
        TopLevel? topLevel = TopLevel.GetTopLevel(this);

        if (topLevel != null)
        {
            var folders = await topLevel.StorageProvider.OpenFolderPickerAsync(new FolderPickerOpenOptions
            {
                AllowMultiple = false,
                Title = "Select Green Hell Modified Save File Output Folder"
            });

            if(folders != null)
            {
                outFolder = OutPath.Text = folders[0].TryGetLocalPath();
            }

        }

    }

    public void ConvertSaveFile(object sender, RoutedEventArgs e)
    {
        if (saveFile == null || outFolder == null) return;

        string[] fileNameParts = saveFile.Split(".");

        string moddedOutFilePath = (fileNameParts.Length > 1) ? 
            fileNameParts[0] + "-modded." + fileNameParts[1] : 
            fileNameParts[0] + "-modded";
        string moddedOutFileName = moddedOutFilePath.Split("\\").Last<string>();

        string outFile = outFolder + "\\" + moddedOutFileName;
        System.Diagnostics.Debug.WriteLine(outFile);

        SaveFileConverter.Load(saveFile);
        if(insectOff.IsChecked == true) {

            SaveFileConverter.DisableInsects();

        }

        SaveFileConverter.Save(outFile);

    }

}
