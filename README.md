
# Table of Contents

1.  [What is this?](#org1fb586c)
2.  [What does it do?](#org0283bc2)
3.  [How far along is this project?](#org2f11018)
4.  [What if I want to change my save file in a way that isn't currently supported?](#orgb696588)
5.  [Is this legal?](#orga318acd)
6.  [How did you know how to make this?](#org7e24054)
7.  [Why did you make this?](#org0d23c5a)



<a id="org1fb586c"></a>

# What is this?

This is the beginning of a project for editing save game files for the game "Green Hell". Since most options are not changeable after creating a save game, the goal is to be able to edit these settings without having to create a new game.


<a id="org0283bc2"></a>

# What does it do?

This application reads in a current save file and outputs a copy of that save file with the modified settings. The program does not change your original save file, however I always recommend backing up your save anyway just to be safe.


<a id="org2f11018"></a>

# How far along is this project?

Very very early. Currently, all this project does is disables "insects", which includes scorpions, arachnids, and centipedes. The core logic for manupulating the save files is present, so future additions should hopefully be relatively quick. 


<a id="orgb696588"></a>

# What if I want to change my save file in a way that isn't currently supported?

Open an issue on this repository with details of what you are wanting changed.


<a id="orga318acd"></a>

# Is this legal?

No code from Green Hell is included in this repository, and is not required for this application to work. The logic for this project is largely based on a reverse engineering of Green Hell's save game logic, but is in no way a duplication and contains no duplicated work.


<a id="org7e24054"></a>

# How did you know how to make this?

I didn't, until I did. I have previous experience in very minor reverse engineering projects, and had knowledge of some tools to use prior to starting this project. I primarily used [ILSpy](https://github.com/icsharpcode/ILSpy) to read the game's logic in the form of decompiled C#/.NET bytecode.


<a id="org0d23c5a"></a>

# Why did you make this?

I started playing the "Spirits of Amazonia" game mode after completing the main game, completely unaware that the maps in this mode contained centipedes. I was also completely unaware I have a visceral reaction to the sight and sound of centipedes, until I had my first surprise encounter with one. It got to the point that I really just didn't want to play the game anymore, just because of the centipedes. I really did not want to restart my playthough just to turn off insects, so I decided I was going to try to turn centipedes off even if the game itself wouldn't let me.

